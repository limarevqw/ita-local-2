<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 * @Route("account")
 */
class AccountController extends AbstractBaseController
{
    /**
     * @Route("/", name="account")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(
        Request $request,
        ObjectManager $manager,
        ApiContext $apiContext,
        UserHandler $userHandler,
        UserRepository $userRepository
    )
    {
        $error = null;
        $message = null;
        /** @var User $user */
        $user = $this->getUser();
        $password = $this->getUser()->getPassword();
        $form = $this->createForm(ProfileType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                if ($user->getPassword()) {
                    $password = $userHandler->encodePassword($user->getPassword());
                }

                $user->setPassword($password);

                $apiContext->updateClientSoc($user->toArray());
                $manager->persist($user);
                $manager->flush();

                $message = "Данные успешно сохранены!";

            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage();
            }
        }

        if ($request->isMethod("POST") && $request->get("token")) {
            try {
                $uloginData = $this->getUloginData($request);
                $network = $uloginData['network'];
                $uid = $uloginData['uid'];

                if ($user->getNetworkUlogin($network)) {
                    $uid = null;
                } else {
                    $user = $userRepository->findOneByUid($uid, $network);

                    if ($user) {
                        throw  new ApiException($network." уже используется другим акаунтом.");
                    }

                    $client = $apiContext->uloginClientExists($uid, $network);

                    if ($client) {
                        throw  new ApiException($network." уже используется другим акаунтом.");
                    }
                }

                $user->setNetworkUlogin($network, $uid);

                $apiContext->updateClientSoc($user->toArray());
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage();
            }
        }

        return $this->render('account/index.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
            'message' => $message
        ]);
    }

    /**
     * @Route("/authorization-successful", name="authorization_successful")
     * @return Response
     */
    public function authorizationSuccessfulAction()
    {
        return $this->render('account/authorization_successful.html.twig');
    }
}