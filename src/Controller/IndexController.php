<?php

namespace App\Controller;

use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractBaseController
{

    /**
     * @Route("/", name="homePage")
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/ping", name="ping")
     * @param ApiContext $apiContext
     * @return Response
     */
    public function pingAction(ApiContext $apiContext)
    {
        try {
            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

}
