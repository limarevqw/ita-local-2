<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthController
 * @package App\Controller
 * @Route("auth")
 */
class AuthController extends AbstractBaseController
{

    /**
     * @Route("/", name="auth")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @return Response
     * @internal param UserHandler $handler
     */
    public function authAction(
        ApiContext $apiContext,
        Request $request,
        UserRepository $userRepository,
        UserHandler $userHandler,
        ObjectManager $manager
    )
    {
        $error = null;
        $form = $this->createForm(LoginType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();

                $password = $userHandler->encodePassword($data['password']);
                $email = $data['email'];

                $user = $userRepository->findOneByPasswordAndEmail($password, $email);

                if ($user) {
                    $userHandler->signIn($user);
                    return $this->redirectToRoute("authorization_successful");
                }

                if (!$apiContext->authClientExists($password, $email)) {
                    throw new ApiException('Пользователь не найден!. Вам нужно зарегистрироватся.');
                }

                $client = $apiContext->getOneDataClient($email);

                if (!$client['status']) {
                    throw new ApiException('Пользователь не найден!. Вам нужно зарегистрироватся.');
                }

                $newUser = $userHandler->createNewUser($client['data']);

                $manager->persist($newUser);
                $manager->flush();

                $userHandler->signIn($newUser);

                return $this->redirectToRoute("authorization_successful");

            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage();
            }
        }

        return $this->render('auth/auth.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    throw new ApiException('Error: Вы уже зарегистрированы. Вам нужно авторизоваться.');
                }

                $data = [
                    'email' => $user->getEmail(),
                    'passport' => $user->getPassport(),
                    'password' => $user->getPassword()
                ];

                $apiContext->createClient($data);

                $user = $userHandler->createNewUser($data);
                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute("auth");
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }

        return $this->render('auth/sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

}