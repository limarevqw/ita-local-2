<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_ONE_CLIENT_DATA = '/get-one-data-client/{email}';
    const ENDPOINT_ULOGIN_CLIENT_EXISTS = '/ulogin-client-exists/{uid}/{network}';
    const ENDPOINT_ULOGIN_DATA_CLIENT = '/ulogin-data-client/{uid}/{network}';
    const ENDPOINT_CREATE_CLIENT_SOC = '/create-client-soc';
    const ENDPOINT_CLIENT_ENCODE_PASSWORD = '/client-encode-password/{password}';
    const ENDPOINT_UPDATE_CLIENT_SOC = '/update-client-soc';
    const ENDPOINT_AUTH_CLIENT_EXISTS = '/auth-client-exists/{password}/{email}';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
                'passport' => $passport,
                'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param string $email
     * @return mixed
     * @throws ApiException
     */
    public function getOneDataClient(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_ONE_CLIENT_DATA, [
            'email' => $email
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $uid
     * @param string $network
     * @return mixed
     * @throws ApiException
     */
    public function getUloginDataClient(string $uid, string $network)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_ULOGIN_DATA_CLIENT, [
            'uid' => $uid,
            'network' => $network
        ]);
        $response = $this->makeQuery($endPoint, self::METHOD_GET);

        return $response['result'];
    }

    /**
     * @param string $uid
     * @param string $network
     * @return bool
     * @throws ApiException
     */
    public function uloginClientExists(string $uid, string $network)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_ULOGIN_CLIENT_EXISTS, [
            'uid' => $uid,
            'network' => $network
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $password
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function authClientExists(string $password, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_AUTH_CLIENT_EXISTS, [
            'password' => $password,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClientSoc(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_CLIENT_SOC, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function updateClientSoc(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_UPDATE_CLIENT_SOC, self::METHOD_PUT, $data);
    }

    /**
     * @param $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword($password)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CLIENT_ENCODE_PASSWORD, [
            'password' => $password
        ]);
        $response = $this->makeQuery($endPoint, self::METHOD_GET);
        return $response['result'];
    }
}
