<?php

namespace App\Model\User;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    protected $container;
    protected $apiContext;

    /**
     * UserHandler constructor.
     * @param ContainerInterface $container
     * @param ApiContext $apiContext
     */
    public function __construct(
        ContainerInterface $container,
        ApiContext $apiContext
    )
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    /**
     * @param array $data
     * @return User
     */
    public function createNewUser(array $data)
    {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $user->setFaceBookId($data['faceBookId'] ?? null);
        $user->setGoogleId($data['googleId'] ?? null);
        $user->setVkId($data['vkId'] ?? null);
        $password = $this->encodePassword($data['password']);
        $user->setPassword($password);
        return $user;
    }

    public function encodePassword($password)
    {
        try {
            return $this->apiContext->encodePassword($password);
        } catch (ApiException $e) {
            return null;
        }
    }

    /**
     * @param User $user
     */
    public function signIn(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}
